import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

# Import models from hats_rest, here.
from hats_rest.models import LocationVO
# from hats_rest.models import Something

def get_locations():
    # if connecting to another docker container, w/in the bridge use
    # the docker alias:port-number/<path>
    # port number - iff docker container 2nd num from docker-compose.yml
    # if not docker container, browser, isomnia, localhost, the 1st number
    response = requests.get("http://wardrobe-api:8000/api/locations/")
    content = json.loads(response.content)
    for location in content["locations"]:
        LocationVO.objects.update_or_create(
            # grabbing the built in locations id from wardrobe and setting it to vo_id from LocationsVO
            vo_id=location["id"],
            name=location["closet_name"]
        )

# any changes to this file, save, the container needs to be restarted, b/c of the while(true)
def poll():
    while True:
        print('Hats poller polling for data')
        try:
            # Write your polling logic, here
            # run a req in this poller to wardrob api to show a list of all the location
            get_locations()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
