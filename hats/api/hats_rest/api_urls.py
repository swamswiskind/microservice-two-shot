from django.urls import path
from .views import api_list_hats, api_view_hat, api_list_locationVO

urlpatterns = [
  path("hats/", api_list_hats, name="api_list_hats"),
  path("hats/<int:pk>/", api_view_hat, name="api_view_hat"),
  path("hats/locationVOs/", api_list_locationVO, name="api_list_locationVO")
]
