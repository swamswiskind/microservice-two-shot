from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Hats, LocationVO

# The encoders are a neccessary to recieve and respond to api requests - json <-> py
class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["name", "vo_id"]

# the id is the auto gen id for each model -  if you need to delete, update, or see detail
# its different than the vo_id
class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "style",
        "color",
        "fabric",
        "picture",
        "id",
        "location"
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    """
    Collection RESTful API handler for hats objects in
    the location.

    GET:
    Returns a dictionary with a single key "hats" which
    is a list of the id, style, fabric, color, picture and location.

    {
        "hats": [
            {
                "id": database id for the hat,
                "style": style number for the hat,
                "fabric": fabric name of the hat,
                "color": color of the hat,
                "picture": a url of a picture of the hat
                "location": {
                        "vo_id": the vo_id of the LocationVO,
                        "name": the closet_name of the LocationVO,
                }
            },
            ...
        ]
    }

    POST:
    Creates a hat and returns its details.
    {
        "style": style number for the hat,
        "fabric": fabric name of the hat,
        "color": color of the hat,
        "picture": a url of a picture of the hat,
        "style": style number for the hat,
        "location": the vo_id of the location,
    }
    """
    if request.method == "POST":
        content = json.loads(request.body)
        try:
            # Get the LocationVO vo_id and put it in the content dict
            location_object = LocationVO.objects.get(vo_id=content["location"])
            content['location'] = location_object

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location vo_id"},
                status=400,
            )

        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsListEncoder,
            safe=False,
        )

    else:
        if request.method == "GET":
            hat = Hats.objects.all()
            return JsonResponse(
                {"hats": hat},
                encoder=HatsListEncoder,
            )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_view_hat(request, pk):
    """
    Single-object API for the hat resource.

    GET:
    Returns the information for a hat resource based
    on the value of pk

    {
        "id": database id for the hat,
        "style": style number for the hat,
        "fabric": fabric name of the hat,
        "color": color of the hat,
        "picture": a url of a picture of the hat
        "location": {
                "vo_id": the vo_id of the LocationVO,
                "name": the closet_name of the LocationVO,
        }
    }

    PUT:
    Updates the information for a hat resource based
    on the value of the pk

    {
        "style": style number for the hat,
        "fabric": fabric name of the hat,
        "color": color of the hat,
        "picture": a url of a picture of the hat
        "location": {
                "vo_id": the vo_id of the LocationVO,
        }
    }

    DELETE:
    Removes the instatnce of this hat resource from the application
    """
    if request.method == "GET":
        try:
            # why id again?
            hat = Hats.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatsListEncoder,
                safe=False
            )
        except Hats.DoesNotExist:
            response = JsonResponse({"message": "Hat does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            hat = Hats.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                {"message": "The hat was deleted"}
            )
        except Hats.DoesNotExist:
            return JsonResponse({"message": "Hat does not exist"})
    else:  # PUT
        try:
            content = json.loads(request.body)
            hat = Hats.objects.get(id=pk)

            # Get the LocationVO vo_id and put it in the content dict
            locations_object = LocationVO.objects.get(vo_id=content["location"])
            content['location'] = locations_object
            props = ["style", "fabric", "color", "picture", "location"]
            for prop in props:
                if prop in content:
                    setattr(hat, prop, content[prop])
            hat.save()
            return JsonResponse(
                hat,
                encoder=HatsListEncoder,
                safe=False,
            )
        except Hats.DoesNotExist:
            response = JsonResponse({"message": "Hat not exist"})
            response.status_code = 404
            return response

"""
    Collection RESTful API handler for hats objects in
    the location. This shold populate our dropdown menu for the hatForm.
    There could be descrepancy w/ the polling and creating a new location
    for when a new hat is created and a location chosen.

    GET:
    Returns a dictionary with a single key "locationVOs" which
    is a list of the vo_id & name.

    {
        "hats": [
            {
                "vo_id": database id for the location
                "name": closet_name for the location
            },
            ...
        ]
    }
"""
# creating an endpoint for HatForm to pull its locationVO for hats
@require_http_methods(["GET"])
def api_list_locationVO(request):
    if request.method == "GET":
        locationVOs = LocationVO.objects.all()
        return JsonResponse(
            {"locationVOs": locationVOs},
            encoder=LocationVODetailEncoder,
        )
