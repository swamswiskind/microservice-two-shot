from django.db import models
from django.urls import reverse
# Create your models here.

# VO - not user defined - reference to what is in wardrobe api
# poller is going to set the vo_id & name - to wardrobe's api location.id (auto created, you don't see it) & closet name, respectively
class LocationVO(models.Model):
  # make sure that whenever you reference ^^ id, you use the vo_id!
  vo_id = models.PositiveSmallIntegerField()
  name = models.CharField(max_length=100)

  def __str__(self):
    return f"{self.name}"
  class Meta:
    #define the singular and plural names for this model in Admin
    verbose_name = "LocationVO"
    verbose_name_plural = "LocationVOs"

class Hats(models.Model):
  style = models.CharField(max_length=100)
  color = models.CharField(max_length=100)
  fabric = models.CharField(max_length=100)
  picture = models.URLField(null=True)
  location = models.ForeignKey(
    LocationVO,
    related_name="hats",
    on_delete=models.PROTECT
  )

  # allows the pk id to be associated with this particular view fn
  def get_api_url(self):
      return reverse("api_show_hat", kwargs={"pk": self.pk})

  # display in admin
  def __str__(self):
      return f"{self.style} - {self.color}/{self.fabric}"

  # displays the order in admin, maybe api_responses - like listing hats
  class Meta:
    ordering = ("style", "color", "fabric")
    #define the singular and plural names for this model in Admin
    verbose_name = "Hat"
    verbose_name_plural = "Hats"

# !!!! any change run migrations in the docker container for hats
