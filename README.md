# Wardrobify

Team:

* Erick - Hats.
* Sam Siskind - Shoes Microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

The shoes microservice is a RESTful API with a set of endpoints for CRUD operations on the shoe object used by Wardrobify.

The shoe object's model is:

class Shoe(models.Model):
    model = models.CharField(max_length=100)
    manufacturer = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.model} - {self.manufacturer}/{self.color}"

    class Meta:
        ordering = ("model", "manufacturer", "color")
        #define the singular and plural names for this model in Admin
        verbose_name = "Shoe"
        verbose_name_plural = "Shoes"

The model for BinVO is used as a value object associated with the Bin object from the Wardrobe API.

To use the Wardrobify react app and test the endpoints, fork the repository, clone it locally and then build and run its associated docker containers. You can import this [Insomnia collection](Insomnia_2023-06-04.json) into your insomnia app to test the endpoints and visit the react app at [http://localhost:3000/](http://localhost:3000/).



## Hats microservice
Explain your models and integration with the wardrobe
microservice, here.

key abbreviations:
  db: database
# Project Description - explaining purpose & functionality
Hat's microservice is a RESTful api designed to allow a user to GET a list of hats, POST a new hat, delete, GET, or modify the details of an existing hat with in our postgres database (db). Each hat is saved with in our wardrobe microservice, which exchanges the hat data with the hat microservice every minute through polling.

# Django Models - Describe the Django models used in your project. Provide details about each model, including the fields, relationships, and any custom methods or properties. You can use code blocks or tables to present the model structure.
We have two models for our hats data: Hats and LocationVO

  The Hats model:
    style = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    fabric = models.CharField(max_length=100)
    picture = models.URLField(null=True)
    location = models.ForeignKey(
      LocationVO,
      related_name="hats",
      on_delete=models.PROTECT
    )
    #allows the pk id to be associated with this particular view fn
    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})

    #display in admin
    def __str__(self):
        return f"{self.style} - {self.color}/{self.fabric}"

    #displays the order in admin, maybe api_responses - like listing hats
    class Meta:
      ordering = ("style", "color", "fabric")
      #define the singular and plural names for this model in Admin
      verbose_name = "Hat"
      verbose_name_plural = "Hats"


The Location model lives on the wardobe api, and the LocationVO is a reference to the Location model. We set the vo_id to the location.id auto generated wardrobe's db.
  LocationVO model:
    vo_id = models.PositiveSmallIntegerField()
    name = models.CharField(max_length=100)

    #to set see the name of the model within admin
    def __str__(self):
      return f"{self.name}"

    class Meta:
      #define the singular and plural names for this model in Admin
      verbose_name = "LocationVO"
      verbose_name_plural = "LocationVOs"

# Installation - List the steps required to install and set up your project locally. Include any dependencies that need to be installed and provide the necessary commands.
We use a docker network to link our microservices. To run docker:
  $ docker-compose build
  $ docker-compose up

'hats_rest.apps.HatsApiConfig', is placed in the hat's project, at the top of the list in INSTALLED_APPS at the file path hats/api/hats_project/settings.
  Migrations need to be made in the hats_api in the docker desktop. Docker file should be running when migrating.
  $ python manage.py makemigrations
  $ python manage.py migrate

To register our models in admin, the following is placed in admin.py in the hats app to be able to see the LocationVO and Hats instances in the admin site. The file path to admin.py is hats/api/hats_rest/admin

  from django.contrib import admin
  from .models import Hats, LocationVO

  @admin.register(LocationVO)
  class LocationVOAdmin(admin.ModelAdmin):
      pass

  @admin.register(Hats)
  class HatsAdmin(admin.ModelAdmin):
      pass




# Configuration - Explain the configuration steps for your Django project. Include information about setting up the database, environment variables, and any other necessary configurations.



# Microservice Integration - Explain how your Django project integrates with a microservice. Describe the purpose of the microservice and its role in your application architecture. Include any relevant configuration or setup instructions for the microservice integration.


# Usage - Provide examples and instructions on how to use your project. This can include information on running the server, accessing the API endpoints, or any other relevant usage details.

To use the terminal with in a docker container's terminal tab, run the following command:
  $ /bin/bash
This will allow you to run standard terminal commands - such as using tab to auto complete
