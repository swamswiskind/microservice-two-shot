import React, { useEffect, useState } from 'react';

function ShoeForm() {

    const [bin, setBin] = useState('');
    const [model, setModel] = useState('');
    const [color, setColor] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [picture_url, setPictureURL] = useState('');

    const [bins, setBins] = useState([])

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/shoes/binvos/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setBins(data.binvos);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {}
    data.model = model;
    data.color = color;
    data.manufacturer = manufacturer;
    data.picture_url = picture_url;
    data.bin = bin;

    const url = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    }

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setModel('');
      setManufacturer('');
      setPictureURL('');
      setBin('');
      setColor('');
    }
  };


    const handleChangeBin = (event) => {
        const value = event.target.value;
        setBin(value);
    }
    const handleChangeColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleChangeManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleChangeModel = (event) => {
        const value = event.target.value;
        setModel(value);
    }

    const handleChangePicture = (event) => {
        const value = event.target.value;
        setPictureURL(value);
    }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new shoe</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">

            <div className="form-floating mb-3">
              <input onChange={handleChangeModel} value={model} placeholder="Model" required type="text" name="model" id="model" className="form-control" />
              <label htmlFor="model">Model</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleChangeManufacturer} value ={manufacturer}placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleChangeColor} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleChangePicture} value={picture_url} placeholder="Picture URL" required type="text" name="pictureurl" id="pictureurl" className="form-control" />
              <label htmlFor="pictureurl">Picture URL</label>
            </div>

            <div className="mb-3">
              <select onChange={handleChangeBin} value={bin} required name="bin" id="bin" className="form-select">
                <option value="">Choose a bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.vo_id} value={bin.vo_id}>{bin.name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default ShoeForm;
