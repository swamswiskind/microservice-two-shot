import React, { useState, useEffect } from "react";
function HatList(props) {

  async function setList(hatList, hatId=null) {
    const url = `http://localhost:8080/api/hats/${hatId}/`;
    const fetchConfig = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      const data = await response.json();
      props.setHatList(hatList.slice().filter(hat => hat.id !== hatId))
    } else {
      console.error("Error in delete");
    }

  }

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Style</th>
          <th>Color</th>
          <th>Fabric</th>
          <th>Picture</th>
          <th>Location</th>
        </tr>
      </thead>
      <tbody>
        {props.hatList.map(hat => {
          return (
            <tr key={hat.id} >
              <td>{ hat.style }</td>
              <td>{ hat.color }</td>
              <td>{ hat.fabric }</td>
              <td>{ hat.picture }</td>
              <td>{ hat.location.name }</td>
              <td>
                <button onClick={() => setList(props.hatList, hat.id)}>
                Delete
                </button>
              </td>
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}

export default HatList;
