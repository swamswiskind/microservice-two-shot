import React, { useState, useEffect } from "react";

function HatForm() {
  const [style, setStyle] = useState("");
  const [fabric, setFabric] = useState("");
  const [color, setColor] = useState("");
  const [picture, setPicture] = useState("");
  const [location, setLocation] = useState("")

  const [locations, setLocations] = useState([]);


  useEffect(() => {
    async function getLocations() {
      const url = "http://localhost:8090/api/hats/locationVOs/";

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        // the key from the from the obj, from the url you fetched
        setLocations(data.locationVOs);
      }
    }
    getLocations();
  }, [])

  async function handleSubmit(event) {
  event.preventDefault();

  const data = {};
  data.style = style;
  data.color = color;
  data.fabric = fabric;
  data.picture = picture;
  data.location = location;

  // console.log(data)
  const url = 'http://localhost:8090/api/hats/';
  const fetchConfig = {
    method: "post",
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
    },
  };

  const response = await fetch(url, fetchConfig);

  if (response.ok) {
    setStyle('');
    setColor('');
    setPicture('');
    setLocation('');
    setFabric('');
  }
}

  function handleChangeLocation(event) {
    const value = event.target.value;
    setLocation(value);
  }

  function handleChangeStyle(event) {
    const value = event.target.value;
    setStyle(value);
  }

  function handleChangeFabric(event) {
    const value = event.target.value;
    setFabric(value);
  }

  function handleChangeColor(event) {
    const value = event.target.value;
    setColor(value);
  }

  function handleChangePicture(event) {
    const value = event.target.value;
    setPicture(value);
  }

  return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Create a Hat</h1>
        <form onSubmit={handleSubmit} id="create-hat">
          <div className="form-floating mb-3">
            <input
              onChange={handleChangeStyle}
              value={style}
              placeholder="style"
              required
              type="text"
              name="style"
              id="style"
              className="form-control"
            />
            <label htmlFor="style">Style</label>
          </div>
          <div className="form-floating mb-3">
            <input
              onChange={handleChangeFabric}
              value={fabric}
              placeholder="fabric"
              required
              type="text"
              name="fabric"
              id="fabric"
              className="form-control"
            />
            <label htmlFor="fabric">Fabric</label>
          </div>
          <div className="form-floating mb-3">
            <input
              onChange={handleChangeColor}
              value={color}
              placeholder="color"
              required
              type="text"
              name="color"
              id="color"
              className="form-control"
            />
            <label htmlFor="color">Color</label>
          </div>
          <div className="form-floating mb-3">
            <input
              onChange={handleChangePicture}
              value={picture}
              placeholder="picture"
              required
              type="text"
              name="picture"
              id="picture"
              className="form-control"
            />
            <label htmlFor="picture">Picture</label>
          </div>
          <div className="mb-3">
            <select
              onChange={handleChangeLocation}
              value={location}
              required
              name="location"
              id="location"
              className="form-select"
            >
              <option value="">Choose a location</option>
              {locations.map((location) => {
                return (
                  <option key={location.vo_id} value={location.vo_id}>
                    {location.name}
                  </option>
                );
              })}
            </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
  )
}

export default HatForm
