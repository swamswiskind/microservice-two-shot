import React, { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeForm from './ShoeForm';
import HatForm from './HatForm';
import ShoeList from './ShoeList';
import HatList from './HatList';

function App() {
  const [ shoeList, setShoeList ] = useState([]);
  const [ hatList, setHatList ] = useState([]);

  async function getHatList() {
    const url = 'http://localhost:8090/api/hats/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setHatList(data.hats);
    }
  }

  async function getShoeList() {
    const url = 'http://localhost:8080/api/shoes/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setShoeList(data.shoes);
    }
  }

  useEffect(() => {
    getHatList();
    getShoeList();
  }, []);


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
        <Route index element={<MainPage />} />
          <Route path="hats">
            <Route path="/hats/list/" element={<HatList hatList={hatList} setHatList={setHatList}/>} />
            <Route path="/hats/new/" element={<HatForm />} />
          </Route>
          <Route path="shoes">
            <Route path="/shoes/list/" element={<ShoeList shoeList={shoeList} />} />
            <Route path="/shoes/new/" element={<ShoeForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
