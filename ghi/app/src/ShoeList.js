function ShoeList(props) {
    const deleteAShoe = async (shoe) => {
        const url = `http://localhost:8080/api/shoes/${shoe.id}/`;
        const fetchConfig = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
        };
        try {
            const response = await fetch(url, fetchConfig);
            if (response.ok) {
                window.location.reload();
                ShoeList();
            }
        } catch (error) {
            console.log('Failed to delete shoe');
        }
    };

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Model</th>
            <th>Color</th>
            <th>Manufacturer</th>
            <th>Picture</th>
            <th>Closet</th>
          </tr>
        </thead>
        <tbody>
          {props.shoeList.map(shoe => {
            return (
              <tr key={shoe.id}>
                <td>{ shoe.model }</td>
                <td>{ shoe.color }</td>
                <td>{ shoe.manufacturer }</td>
                <td><img width={40} src={ shoe.picture_url } alt={shoe.model}/></td>
                <td>{ shoe.bin.name }</td>
                <td>
                    <button onClick={() => deleteAShoe(shoe)}>
                    Delete
                    </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default ShoeList;
